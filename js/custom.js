// JS Init
jQuery(document).ready(function($) {
    app.init();
});


var app = {
    el: {
        $barcode: $('#barcode'),
        $barcodeInput: $('#send-barcode'),
        $part_number: $('#part_number'),
        $serial_number: $('#serial_number'),
        $date: $('#date'),
        $user: $('#user'),
        $status: $('#status'),
        $db: $('#db').val(),
        $passBlk: $('.pass-blk'),
        $table: $('#example').DataTable({
            "searching": false,
            "lengthChange": false,
            "info": false
        })
    },
    data: '',

    init: function() {
        app.el.$barcode.focus();
        app.el.$barcodeInput.on('click', function(e) {
            e.preventDefault();
            app.sendBarcode($('#send-barcode'));
        })

        app.el.$barcodeInput.on('keyup', function(e) {
            if (e.which == 9) {
                app.sendBarcode($('#send-barcode'));
            }
        })

        $('#testResult, .js-view-pass').on('click', function(e) {
            e.preventDefault();
            app.testResult();
        })


        this.showDetails();
    },
    sendBarcode: function() {
        var barcode = app.el.$barcode.val().split(":");
        $('.error-msg').hide();

        if (barcode[2] != undefined) {
        $.ajax({
                url: 'api.php',
                type: 'POST',
                data: { action: 'scanBarcode', barcode: barcode[2], db: app.el.$db },
            })
            .done(function(data) {
                console.log(data);
                if (data != 0) {
                    data = JSON.parse(data);
                    app.el.$part_number.val(data[0].Model_Number);
                    app.el.$serial_number.val(data[0].Serial_Number);
                    app.el.$date.val(data[0].Test_Date.date);
                    app.el.$user.val(data[0].Username);
                    app.el.$status.val(data[0].Status);
                    app.el.$passBlk.hide();
                    if (data[0].Status == "FAIL") {
                        app.showMsg('.fail-msg');
                        app.el.$status.val(0);
                        app.testResult();
                    } else if (data[0].Status == "PASS" || data[0].Status == "ALLPASS") {
                        app.showMsg('.pass-msg');
                        $('.dataTables_wrapper').hide();
                        app.el.$passBlk.show();
                    }
                } else {
                    $('.error-msg').show();
                }

            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
        } else {
            $('.error-msg').show();
        }
    },
    testResult: function() {

        $.ajax({
                url: 'api.php',
                type: 'POST',
                data: { action: 'testResult', serial: app.el.$serial_number.val(), status: app.el.$status.val(), db: app.el.$db },
            })
            .done(function(data) {
                data = JSON.parse(data);
                app.data = data;
                app.el.$table.clear();
                $.each(data, function(index, val) {
                    app.el.$table.row.add([index, val[0]['measurement'], val[0]['status'], val[0]['lo'], val[0]['value'], val[0]['hi'], val[0]['unit']]).draw();
                });
                $('.dataTables_wrapper').show();

            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });

    },
    formatDetails: function(data) {
        $html = '<table class="child-table"><thead> <tr> <th>Test Name</th> <th>Measurement</th> <th>Results</th> <th>Lo</th> <th>Value</th> <th>Hi</th> <th>Unit</th> </tr> </thead>';
        $.each(data, function(index, val) {
            $html += '<tr>';
            $html += '<td></td>';
            $html += '<td>' + val['measurement'] + '</td>';
            $html += '<td>' + val['status'] + '</td>';
            $html += '<td>' + val['lo'] + '</td>';
            $html += '<td>' + val['value'] + '</td>';
            $html += '<td>' + val['hi'] + '</td>';
            $html += '<td>' + val['unit'] + '</td>';

            $html += '</tr>';
        });
        $html += '</table>';
        return $html;
    },
    showDetails: function() {
        $('#example tbody').on('click', 'td', function() {
            var tr = $(this).closest('tr');
            var row = app.el.$table.row(tr);

            console.log();

            if (row.child.isShown()) {
                row.child.hide();
                tr.removeClass('shown');
            } else {
                row.child(app.formatDetails(app.data[row.data()[0]])).show();
                tr.addClass('shown');
            }
        });
    },
    showMsg: function(el) {
        $('.msg-blk *').hide();
        $(el).show();
    }
}